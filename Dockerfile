ARG BASEIMG=alpine:3.8

FROM golang:1.11-alpine3.8 as gobuild
ARG VERSION=v0.14.0
ARG GOOS=linux
ARG GOARCH=amd64

RUN apk add -U git make

WORKDIR /go/src
RUN git clone -b ${VERSION} https://github.com/kubernetes-incubator/bootkube.git /go/src/github.com/kubernetes-incubator/bootkube

WORKDIR /go/src/github.com/kubernetes-incubator/bootkube
# Upstream Makefile cross-compile is buggy
#RUN make _output/bin/linux/${ARCH}/checkpoint
RUN \
  go build -ldflags "-s -w -X github.com/kubernetes-incubator/bootkube/pkg/version.Version=$(./build/git-version.sh)" \
  -o /checkpoint \
  github.com/kubernetes-incubator/bootkube/cmd/checkpoint

FROM ${BASEIMG}

COPY --from=gobuild /checkpoint /usr/local/bin/

CMD ["checkpoint"]
